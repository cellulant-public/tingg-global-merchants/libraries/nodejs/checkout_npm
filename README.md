# Cellulant.io : Checkout Encryption
Node Module to Encrypt Payload 
[`Get Started`](https://cellulant-prod-portal.apigee.io/libraries/node)

---

To ensure the highest level of security, Cellulant accepts encrypted data in checkout request. 

Node.js is a run-time environment that only runs on a server. Encrpted module/library can be use with any node Js framework. In below example, we will use the [Express framework](https://expressjs.com/) to create a realistic use case.

## Project setup instructions

Create an Express app if you do not have one using the following commands. A more in-depth guide can be found [here](https://expressjs.com/en/starter/installing.html).

**Step 1**: Create a project directory 
```
$ mkdir checkout-demo
```

**Step 2**:  Go into the project directory. 
```
$ cd checkout-demo
```

**Step 3**:  Initialize an NPM project accepting all prompts. 
```
$ npm init -y . 
```

**Step 4**:   Install the express framework. 
```
$ npm install express --save 
```

**Step 5**:   Install the encryption library. 
```
$ npm install @cellulant/checkout_encryption
```

**Step 6**:   Sample code to encrpt the payload.

```
// File name : CheckoutEncryptionTest.js

let checkoutEncrypt = require('@cellulant/checkout_encryption');

const accessKey = "<YOUR_ACCESS_KEY>"
const IVKey = "<YOUR_IV_KEY>";
const secretKey = "<YOUR_SECRET_KEY>";
const algorithm = "aes-256-cbc";

// sample payload object 
var payloadObj = {
             "merchant_transaction_id":"ABCD12345",
             "customer_first_name":"Test",
             "customer_last_name":"User",
             "msisdn":123456,
             "customer_email":"testuser@gmail.com",
             "request_amount":10.0,
             "currency_code":"KEH",
             "account_number":"12345",
             "service_code":"JOHNDOEONLINE",
             "due_date":"2022-01-07 00:00:00",
             "request_description":"Test payment",
             "country_code":"KEN",
             "language_code":"EN",
             "success_redirect_url":"http://abc.com/success",
             "fail_redirect_url":"http://abc.com/fail",
             "pending_redirect_url":"http://abc.com/pending",
             "callback_url":"http://abc.com/callback",
             "charge_beneficiaries":null
    };

	const payloadStr = JSON.stringify(payloadObj);
 
  // Create object of the Encryption class  
  let encryption = new checkoutEncrypt.Encryption(IVKey, secretKey, algorithm);

 // call encrypt method
 var result = encryption.encrypt(payloadStr);

 // print the result
 console.log(result);

```

**Step 7**:  Open a terminal session in your project directory and run the application using the command 
```
$ node CheckoutEncryptionTest.js
```

---


## References
- [Getting Started](https://cellulant-prod-portal.apigee.io/libraries)
- [Express checkout payload](https://cellulant-prod-portal.apigee.io/express-checkout-redirect)
